from django.contrib import admin
from cars.models import Car



from django.utils.html import format_html



class MyModelAdmin(admin.ModelAdmin):
    search_fields = ['title']
    list_filter = ['year_of_issue']
    list_display = ('title', 'image')
    filter_horizontal = ('model_id', 'brand_id')

    def image(self,obj):
        try:
            return format_html('<img src="{0}" style="width: 68px; height: 45px; object-fit: cover;" />'.format(obj.photo.url))
        except:
            return format_html('<strong>Изображения нет</strong>')

admin.site.register(Car, MyModelAdmin)