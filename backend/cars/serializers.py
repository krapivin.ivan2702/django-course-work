from rest_framework import serializers
from cars.models import Car
from authentication.models import User
from brands.models import Brand
from models.models import Model

class UserSerializerForAd(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name']

class BrandSerializerForAd(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ['name']

class ModelSerializerForAd(serializers.ModelSerializer):
    class Meta:
        model = Model
        fields = ['name']  

class AdSerializer(serializers.ModelSerializer):
    user_name = UserSerializerForAd(source='user_id', many=True)
    brand_name = BrandSerializerForAd(source='brand_id', many=True)
    model_name = ModelSerializerForAd(source='model_id', many=True)
    class Meta:
        model = Car
        # fields = '__all__'
        exclude = ['user_id', 'brand_id', 'model_id']




