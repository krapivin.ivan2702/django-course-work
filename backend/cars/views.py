from rest_framework.viewsets import ModelViewSet
from cars.serializers import AdSerializer
from cars.models import Car

class AdViewSet(ModelViewSet):
    queryset = Car.objects.all()
    serializer_class = AdSerializer
