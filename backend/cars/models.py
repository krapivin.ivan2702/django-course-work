from turtle import title
from django.db import models
from authentication.models import User
from brands.models import Brand
from models.models import Model

class Car(models.Model):
    user_id = models.ManyToManyField(verbose_name='ID пользователя', to=User, related_name='Cars')
    title = models.CharField(verbose_name='Название объявления', max_length=255, default="test")
    cost = models.IntegerField(verbose_name='Цена', max_length=255, default=0)
    brand_id = models.ManyToManyField(verbose_name='ID бренда', to=Brand, related_name='Cars')
    model_id = models.ManyToManyField(verbose_name='ID модели', to=Model, related_name='Cars')
    engine_volume = models.IntegerField(verbose_name='Лошадиных сил', max_length=255)
    year_of_issue = models.IntegerField(verbose_name='Год выпуска', max_length=4)
    color = models.CharField(verbose_name='Цвет', max_length=255)
    photo = models.ImageField(verbose_name='Фото', upload_to='cars/photos')
    description = models.TextField(verbose_name='Текст описания')
    phone_number = models.CharField(verbose_name='Телефон для связи', max_length=255)

    def __str__(self):
        return self.title


    class Meta:
        verbose_name = 'Объявление авто'
        verbose_name_plural = 'Объявления авто'
