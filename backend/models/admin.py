from django.contrib import admin
from models.models import Model

class MyModelAdmin(admin.ModelAdmin):
    search_fields = ['name']
    list_filter = ['brand_id']

admin.site.register(Model, MyModelAdmin)