from rest_framework import serializers
from models.models import Model
from brands.models import Brand

class BrandSerializerForModel(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ['name']

class ModelSerializer(serializers.ModelSerializer):
    brand_name = BrandSerializerForModel(source='brand_id', many=True)
    class Meta:
        model = Model
        fields = '__all__'

