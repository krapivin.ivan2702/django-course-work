from django.db import models
from brands.models import Brand

class Model(models.Model):
    brand_id = models.ManyToManyField(verbose_name='ID бренда', to=Brand, related_name='Models')
    name = models.CharField(verbose_name='Название модели', max_length=255)

    def __str__(self):
        return self.name


    class Meta:
        verbose_name = 'Модель'
        verbose_name_plural = 'Модели'
