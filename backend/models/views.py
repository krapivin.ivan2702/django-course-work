from rest_framework.viewsets import ModelViewSet
from models.serializers import ModelSerializer
from models.models import Model

class ModelViewSet(ModelViewSet):
    queryset = Model.objects.all()
    serializer_class = ModelSerializer
