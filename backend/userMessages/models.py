from django.db import models
from authentication.models import User
from cars.models import Car

class Message(models.Model):
    user_id = models.ManyToManyField(verbose_name='ID пользователя', to=User, related_name='Messages')
    ad_id = models.ManyToManyField(verbose_name='ID объявления', to=Car, related_name='Messages')
    text = models.TextField(verbose_name='Текст сообщения')

    def __str__(self):
        return self.text


    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'