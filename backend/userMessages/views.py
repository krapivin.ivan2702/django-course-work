from rest_framework.viewsets import ModelViewSet
from userMessages.serializers import MessageSerializer
from userMessages.models import Message

class MessageViewSet(ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
