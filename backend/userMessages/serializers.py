from rest_framework import serializers
from userMessages.models import Message
from authentication.models import User

class UserSerializerForMessage(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name']

class MessageSerializer(serializers.ModelSerializer):
    user_name = UserSerializerForMessage(source='user_id', many=True)
    class Meta:
        model = Message
        fields = '__all__'

