from django.contrib import admin
from brands.models import Brand

class MyModelAdmin(admin.ModelAdmin):
    search_fields = ['name']

admin.site.register(Brand, MyModelAdmin)