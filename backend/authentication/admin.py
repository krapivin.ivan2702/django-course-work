from django.contrib import admin
from authentication.models import User

from django.utils.html import format_html

class MyModelAdmin(admin.ModelAdmin):
    search_fields = ['email']
    list_display = ('email', 'image')

    def image(self,obj):
        try:
            return format_html('<img src="{0}" style="width: 68px; height: 45px; object-fit: cover;" />'.format(obj.photo.url))
        except:
            return format_html('<strong>Изображения нет</strong>')


admin.site.register(User, MyModelAdmin)


